#Introduction
Role classes module is used to define classes for each role
in the body tag of the page.

##Installation
Role classes can be installed easily.

###Composer installation:
Copy the composer command from the module page and run using
terminal from the root path.
Go to Admin > Extend and enable the module.

####Manuall installation:
Download the module and put to your root/modules/contribe/ directory,
or where ever you install contrib modules on your site.
Go to Admin > Extend and enable the module.
