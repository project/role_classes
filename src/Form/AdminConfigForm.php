<?php

namespace Drupal\role_classes\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class LoginRedirectionForm.
 *
 * @package Drupal\redirect_after_login\Form
 */
class AdminConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'role_classes_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('role_classes.settings');
    $saved_classname = $config->get('class');

    $form['roles'] = [
      '#type'  => 'fieldset',
      '#title' => $this->t('All roles'),
    ];
    foreach (user_role_names() as $user => $name) {
      $form['roles'][$user] = [
        '#type'          => 'textfield',
        '#title'         => $name,
        '#size'          => 60,
        '#maxlength'     => 128,
        '#description'   => $this->t('Add a class name'),
        '#required'      => FALSE,
        '#default_value' => isset($saved_classname[$user]) ? $saved_classname[$user] : '',
      ];
    }

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type'        => 'submit',
      '#value'       => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $loginUrls = [];
    foreach (user_role_names() as $user => $name) {
      $role_classes[$user] = $form_state->getValue($user);
      $form_state->getValue($user);
    }
    $this->config('role_classes.settings')
      ->set('class', $role_classes)
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Get Editable config names.
   *
   * @inheritDoc
   */
  protected function getEditableConfigNames() {
    return ['role_classes.settings'];
  }

}
